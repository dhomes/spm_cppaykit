// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let major=1
let minor=0
let point=10007

let package = Package(
    name: "CPPayKit",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "CPPayKit",
            targets: ["CPPayKit"]),
    ],
    dependencies: [
		.package(url: "https://bitbucket.org/dhomes/spm_cpcorekit.git", branch: "master"),
		.package(url: "https://bitbucket.org/dhomes/spm_cpcloudkit.git", branch: "master"),
        .package(url: "https://bitbucket.org/dhomes/spm_cploginkit.git", branch: "master")
    ],
    targets: [
        .binaryTarget(
            name: "CPPayKit",
            url: "https://bitbucket.org/dhomes/spm_cppaykit/downloads/CPPayKit.xcframework.zip",
            checksum: "344d6c94014ffb09f7a625672f31b355c3fa9e154ec3e57b9924c8389e10c9ab"
        )
    ]
)
