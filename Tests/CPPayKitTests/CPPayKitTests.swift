import XCTest
@testable import CPPayKit

final class CPPayKitTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(CPPayKit().text, "Hello, World!")
    }
}
